package main

import "fmt"

func say(text string, c chan<- string) {
	c <- text
}

func message(text string, c chan<- string) {
	c <- text

}

func main() {
	c := make(chan string, 1)
	c1 := make(chan string, 2)

	c1 <- "Mensaje 1"
	c1 <- "Mensaje 2"

	fmt.Println(len(c1), cap(c1))

	// Ejemplificar with range y close
	close(c1)
	// da un error debido al close y además de adiccionar un valor que no esta en la capacidad
	// c1 <- "Mensaje 3"
	// Range se usa cuando quieres iterar y recorrer
	// for message := range c1 {
	// 	fmt.Println(message)
	// }

	// select
	email := make(chan string)
	email2 := make(chan string)

	go message("mensaje1", email)
	go message("Mensaje 2", email2)

	for i := 0; i < 2; i++ {
		// Aqui se usa el select
		select {
		case m1 := <-email:
			fmt.Println("Email recibido de email", m1)
		case m2 := <-email2:
			fmt.Println("Email recibido de email2", m2)
		}
	}

	fmt.Println("Hello")

	go say("bye", c)

	fmt.Println(<-c)
}
