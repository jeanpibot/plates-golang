package main

import "fmt"


func main() {
	// estructuras de datos

	// array
	var array[4]int
	fmt.Println(array)
	array[0] = 1
	array[1] = 2
	fmt.Println(array)
	fmt.Println(array, len(array), cap(array))

	// Slice
	slice := []int{1, 2,5, 10, 9, 11}
	fmt.Println(slice, len(slice), cap(slice))
}