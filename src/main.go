package main

import "fmt"

type pc struct {
	ram int
	disk int
	brand string
}

func (p pc) ping() {
	fmt.Println(p.brand, "pong")
}

func (p *pc) duplicateRAM() {
	p.ram = p.ram * 2
}
// sirve para imprimir mensajes de una manera más optima
func (p pc) String() string {
	return fmt.Sprintf("Tengo %d GB RAM, %d GB disco y es un %s", p.ram, p.disk, p.brand)
}

func main() {
	myPc := pc{
		ram: 16,
		disk: 500,
		brand: "Macbook air",
	}

	fmt.Println(myPc)

	myPc.ping()

	fmt.Println(myPc)
	myPc.duplicateRAM()

	fmt.Println(myPc)
	myPc.duplicateRAM()

	fmt.Println(myPc)

}